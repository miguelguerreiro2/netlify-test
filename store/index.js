export const state = () => ({
    device: {},
    menu: false,
    contentMounted: false,
})

export const mutations = {
    setDevice(state, value){
        state.device = value
    },
    setMenu(state, value){
      state.menu = value
    },
    setContentMounted(state, value){
      state.contentMounted = value
    }
}

export const getters = {
  getMenu(state){
    return state.menu
  },
  getContentMounted(state){
    return state.contentMounted
  }
}
