export const state = () => ({
  error: {
    nome: false,
    nome_e_apelido: false,
    email: false,
    tel: false,
    mensagem: false
  },
})

export const getters = {
  getAllErrors(state) {
    if (state.error.name == false && state.error.email == false && state.error.tel == false && state.error.mensagem == false) {
      return false;
    }
    return true;
  },
  getErrorName(state) {
    return state.error.nome
  },
  getErrorNameApelido(state) {
    return state.error.nome_e_apelido
  },
  getErrorEmail(state) {
    return state.error.email
  },
  getErrorTel(state) {
    return state.error.tel
  },
  getErrorMensagem(state) {
    return state.error.mensagem
  }
}

export const mutations = {
  setErrorNome(state, value) {
    state.error.nome = value
  },
  setErrorNameApelido(state, value) {
    state.error.nome_e_apelido = value
  },
  setErrorEmail(state, value) {
    state.error.email = value
  },
  setErrorTel(state, value) {
    state.error.tel = value
  },
  setErrorMensagem(state, value) {
    state.error.mensagem = value
  }
}
