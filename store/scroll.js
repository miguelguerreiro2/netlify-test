export const state = () => ({
  space:false
})

export const getters = {
  getSpace(state) {
   return state.space
  },
}

export const mutations = {
  setSpace(state, value) {
    state.space = value
  },
}
