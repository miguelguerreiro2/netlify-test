export const state = () => ({
    counter: 0,
    answers:[],
    data:[]
})
  
export const mutations = {
    increment(state) {
      state.counter++
    },
    decrease(state) {
        state.counter--
    },
    add(state, value) {
        state.answers.push({
            name: value.id, 
            value: value.value,
            condition: value.condition,
        })
    },
    remove(state) {
        state.answers.pop()
    },
    data(state, value){
        state.data.push({
            id: value.id, 
            name: value.name,
            email: value.email,
            phone: value.phone
        })
    },
}
  