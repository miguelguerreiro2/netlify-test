const baseUrl = 'https://dev-just-smile-backoffice.pantheonsite.io'
//const feUrl = process.env.FE_URL
import path from 'path'
import fs from 'fs'

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'nuxt-justsmile',
    htmlAttrs: {
      lang: 'pt'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    // SCSS file in the project
    './assets/scss/styles.scss',
    'slick-carousel/slick/slick.css',
    'slick-carousel/slick/slick-theme.css'
  ],

  target: 'static',

  generate: {
    routes: ['/']
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    // '@/plugins/device.server.js',
    { src: '~~/node_modules/vue-rellax/lib/nuxt-plugin', ssr: false },
    { src: '@/plugins/calendar.js', ssr: false },
    { src: '@/plugins/slick.js', ssr: false },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: [{
    path: '~/components',
    global: true,
  }],

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
      '@/modules/sitemapRouteGenerator',
    [
      '~/modules/search-api-lunr',
      {
        server: 'druxt',
        index: 'default',
      },
    ],
    'druxt-auth',
    '@nuxtjs/device'
  ],

  device:{
    refreshOnResize: true

  },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    'bootstrap-vue/nuxt',
    '@nuxtjs/style-resources',
    // '@privyid/nuxt-csrf',
    'druxt',
    'druxt-entity',
    'druxt-site',
    'druxt-blocks',
    'druxt-views',
    '@nuxtjs/gtm',
    'druxt-router/nuxt',
    '@nuxtjs/axios',
    'cookie-universal-nuxt',
    '@nuxtjs/robots',
    '@nuxtjs/sitemap',
    [
      '@nuxtjs/lunr-module',
      {
        fields: [
          'title_transliterated',
          'category_transliterated',
          'tags_transliterated',
          'body_transliterated',
          'title_transliterated',
          'category_transliterated',
          'tags_transliterated',
          'body_transliterated',
        ],
          languages:false,
      },
    ],


    ['nuxt-gmaps', {
      key: 'AIzaSyBlKEVFY3LZ9OWTa8E0TX23jhxQtdv9umY',
    }],
  ],
  gtm: {
    id: 'GTM-WMCPQ8D'
  },
  robots: {
    UserAgent: '*',
  },
  sitemap: {
    hostname: 'https://justsmile.pt',

  },
  druxt: {
    baseUrl,
    blocks: {
      query: { fields: [] },
    },
    proxy: {
      api: true
    },
    site: {
      theme: 'justsmile'
    },
    views: {
      query: { bundleFilter: true },
    },
    auth: {
      clientId: 'af8a552b-2347-49ed-b110-349dcfc0241b'
    },
  },

  publicRuntimeConfig: {
    baseUrl
  },

  bootstrapVue: {
    bootstrapCSS: false,
    bootstrapVueCSS: false,
    componentPlugins: [],
    directivePlugins: []
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  },

  server: {
    port: 8000,
    // https: {
    //   key: fs.readFileSync(path.resolve(__dirname, 'server.key')),
    //   cert: fs.readFileSync(path.resolve(__dirname, 'server.crt'))
    // }
  },

  proxy: {
    '/jsonapi': baseUrl
  },

  axios: {
    baseURL: baseUrl,
    credentials: true,
  },

  styleResources: {
    scss: [
      '~/assets/scss/variables.scss',
    ]
  },
}
